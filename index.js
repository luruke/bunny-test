const draw = require("./draw");

// canvas.set(10, 10, 1);

// console.log(canvas.frame())

const bunny = require("./bunny");
const glmatrix = require("./gl-matrix");
const width = process.stdout.columns;
const height = process.stdout.rows * 2 - 5;
const mat4 = glmatrix.mat4;
const vec3 = glmatrix.vec3;

const canvas = new draw(
  // process.stdout.columns * 2 || 80,
  // process.stdout.rows || 24
  width,
  height
);

let points = [];
bunny.cells.forEach(cell => {
  points.push(bunny.positions[cell[0]]);
  points.push(bunny.positions[cell[1]]);
  points.push(bunny.positions[cell[2]]);
});

const project = mat4.create();
mat4.ortho(project, width / -2, width / 2, height / -2, height / 2, 0, 5);

const LIGHT = [-4, 4, 0];

function f() {
  canvas.clear();

  const angle = +new Date() * 0.001;
  let mview = [
    Math.cos(angle),
    0,
    -Math.sin(angle),
    0,

    0,
    1,
    0,
    0,

    Math.sin(angle),
    0,
    Math.cos(angle),
    0,

    0,
    0,
    0,
    1
  ];

  const m = [];
  mat4.mul(m, project, mview);

  points.forEach(point => {
    let o = [];
    vec3.transformMat4(o, point, m);
    let x = ((o[0] * 8 + 1) * width) / 2;
    let y = (-o[1] * 8 + 2) * height;
    let z = (o[2] + 2) / 2;

    z = Math.min(1, Math.max(0, z));

    // console.log(z);
    // canvas.set(x, y / 2, z, [255 * z, 255 * z, 255 * z])
    canvas.set(x, y / 2, z, [255 * z, 0, 0]);
  });

  console.log("\x1b[1;1H" + canvas.frame());
  // process.stdout.write(canvas.frame())
}

f();

setInterval(f, 16);

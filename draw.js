const LOWER_HALF_BLOCK = "\u2584";

module.exports = class Draw {
  constructor(w, h) {
    this.w = w;
    this.h = h;
    this.pixels = new Array(this.w * this.h);
    this.bg = [0, 0, 0, -100];
    this.clear();
  }

  set(x, y, depth, color) {
    let coord = [Math.floor(x - 1), Math.floor(y - 1)];

    let i = coord[1] * this.w + coord[0];

    color = color.map(item => {
      return Math.min(255, Math.max(0, Math.floor(item)));
    });

    if (this.pixels[i] && depth > this.pixels[i][3]) {
      this.pixels[i] = color;
      this.pixels[i][3] = depth;
    }
  }

  clear() {
    this.pixels.fill(this.bg);
  }

  frame() {
    let frame = "";

    for (let y = 0; y < this.h; y += 2) {
      for (let x = 0; x < this.w; x++) {
        const pixelUp = this.pixels[y * this.w + x];
        let pixelDown = this.pixels[(y - 1) * this.w + x];

        if (!pixelDown) {
          pixelDown = this.bg;
        }

        // Set fg
        frame += `\x1b[38;2;${pixelUp[0]};${pixelUp[1]};${pixelUp[2]}m`;
        // Set bg
        frame += `\x1b[48;2;${pixelDown[0]};${pixelDown[1]};${pixelDown[2]}m`;
        frame += LOWER_HALF_BLOCK;
        // Reset color
        frame += "\x1b[0m";
        frame += "\x1b[0m";
        frame += "\x1b[0m";
        // frame += '\x1b[0m';

        // if ((i + 1) % this.w === 0) {
        //   frame += '\n';
        // }
      }

      frame += "\n";
    }

    return frame;
  }
};
